import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../components/my_button.dart';
import '../components/my_textfield.dart';
import '../main.dart';
import '../pages/home_page.dart';

class NomAbonne extends StatefulWidget {
  const NomAbonne({super.key});

  @override
  State<NomAbonne> createState() => _NomAbonneState();
}

class _NomAbonneState extends State<NomAbonne> {

  final TextEditingController TiktokController = TextEditingController();
  final TextEditingController FacebookController = TextEditingController();
  final TextEditingController instagramController = TextEditingController();


  Future<void> addNomCompte() async
  {
    final username = supabase.auth.currentUser!.userMetadata!['username'];
    final userId = supabase.auth.currentUser!.id;
    final numero = supabase.auth.currentUser!.userMetadata!['numero'];
    final parrain = supabase.auth.currentUser!.userMetadata!['parrain'];
    final email = supabase.auth.currentUser?.email;
    //final username = supabase.auth.currentUser.userMetadata
   // print(sessions);
    await supabase.from('profiles').update({
      'nom_user_tiktok': TiktokController.text.trim(),
      'nom_user_instagram': instagramController.text.trim(),
      'nom_user_facebook': FacebookController.text.trim(),
      'email': email.toString(),
      'username': username,
      'parrain': parrain,
      'numero': numero
    }).eq('id', userId);
    if(!mounted) return;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomePage()),);
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 50,),
                MyTextField(
                  controller: TiktokController,
                  hintText: 'Nom Profil Tiktok',
                  obscureText: false,
                ),
                SizedBox(height: 20,),
                MyTextField(
                  controller: FacebookController,
                  hintText: 'Nom Profil Facebook',
                  obscureText: false,
                ),
                SizedBox(height: 20,),
                MyTextField(
                  controller: instagramController,
                  hintText: 'Nom Profil Instagram',
                  obscureText: false,
                ),
                SizedBox(height: 20,),
                MyButton(
                  onTap: () async {
                    await addNomCompte();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
