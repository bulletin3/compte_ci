
import 'package:compte/model/bonus.dart';
import 'package:compte/model/expiration.dart';
import 'package:compte/model/type.dart';
import 'package:compte/model/typeprice.dart';
import 'package:compte/model/user.dart';
import 'package:compte/utils/app_globals.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../main.dart';
import '../model/like_profil.dart';
import '../model/pay.dart';
import '../model/retrait.dart';



//final messaging = FirebaseMessaging.instance;

class NoteRepository {
  final String tableName = 'like';

  //solde gestion
  Future<List<Profiles>> getUserSolde() async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('profiles')
        .select().eq('id', userId);
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Profiles(
        id: data['id'].toString(),
        abonne: data['abonne'],
        solde: data['solde'],
        age: data['age'],
        centre: data['centre'],
        dateAni: data['dateAni']
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  //get post
  Future<List<Likes>> getLike(int type) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('likes')
        .select('*, type(*), typeprice(*)').eq('type_id', type);
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Likes(
        id: data['id'],typeprice: Typeprice.fromJson(data['typeprice']),
        username: data['username'].toString(),
        content: data['content'].toString(),
        type: Type.fromJson(data['type']),
        age: data['age'],
        created_at: data['created_at'] ,
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }
  //aldulte
  Future<List<Likes>> getLikeAldulte(int type) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('likes')
        .select('*, type(*), typeprice(*)').eq('type_id', type).eq('age', false);
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Likes(
        id: data['id'],typeprice: Typeprice.fromJson(data['typeprice']),
        username: data['username'].toString(),
        content: data['content'].toString(),
        type: Type.fromJson(data['type']),
        age: data['age'],
        created_at: data['created_at'] ,
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  Future<List<Like_profil>> getLikeProfile( ) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('like_profil')
        .select('*, likes(*)').eq('profiles_id', userId).eq('payer', true);
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Like_profil(
        id: data['id'],
        activer: data['activer'] as bool,
        payer: data['payer'] as bool,
        //profiles: Profiles.fromJson(data['profiles']),
        likes: Likes.fromJson(data['likes']),
        created_at: data['created_at'] ,
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }



  //remplacer


  Future<List<Likes>> getAutoLike(userId , like_id ) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('like_profil')
        .insert({'profiles_id': userId, 'likes_id': like_id});
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Likes(
        id: data['id'],
        username: data['username'].toString(),
        content: data['content'].toString(),
        type: Type.fromJson(data['type']),
        created_at: data['created_at'],
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }



  //voirs si active


  Future<List<Like_profil>> getVoirActive(userId , like_id ) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('like_profil').select('*, likes(*), profiles(*)').eq('profiles_id', userId).eq('likes_id', like_id);
        //.eq({'profiles_id': userId, 'likes_id': like_id});
    print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Like_profil(
        id: data['id'],
        activer: data['activer'],
        payer: data['payer'],
        profiles: Profiles.fromJson(data['profiles']),
        likes: Likes.fromJson(data['likes']),
        created_at: data['created_at'] ,
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }




  //fin Solde

  //logique de like + solde


  Future<List<Profiles>> getSoldeLike(idUser, solde) async {
    //final response = await Supabase.client.from('post').select().execute();
    final response =
    await supabase.from('profiles').update({
      'solde': solde,
    }).eq('id', idUser);
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Profiles(
        id: data['id'].toString(),
        abonne: data['abonne'],
        solde: data['solde'],
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  // Pass like a activer
  Future<List<Likes>> getLikeactiver(int type) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('likes')
        .select('*, type(*)').eq('type_id', type);
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Likes(
        id: data['id'],
        username: data['username'].toString(),
        content: data['content'].toString(),
        type: Type.fromJson(data['type']),
        created_at: data['created_at']  ,
        //profiles:  Profiles.fromJson(data['profiles']),
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  //retrait
  Future<List<Retrait>> getRetrait(montant,numero, mode ) async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('retrait')
        .insert({'profiles_id': userId, 'montant': montant, 'numero': numero, 'mode': mode});
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Retrait(
        id: data['id'],
        montant: data['montant'],
        numero: data['numero'],
        payer: data['payer'],
        mode: data['mode']
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  //moin

//voir les retrait

  Future<List<Retrait>> getRetraitAll() async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('retrait')
        .select().eq('profiles_id', userId);
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Retrait(
          id: data['id'],
          montant: data['montant'],
          numero: data['numero'],
          payer: data['payer'],
          mode: data['mode']
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  Future<List<Retrait>> getRetraitAll1() async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('retrait')
        .select().eq('profiles_id', userId).eq('payer', false);
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Retrait(
          id: data['id'],
          montant: data['montant'],
          numero: data['numero'],
          payer: data['payer'],
          mode: data['mode']
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  // Expire

//Espire
  Future<List<Expiration>> getExpire() async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('expiration')
        .select('*');
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Expiration(
        id: data['id'],
        Heure: data['Heure'],
        Date: data['Date'],
        created_at: data['created_at'] ,
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }

  //bonus

//read Bonus
  Future<List<Bonus>> getBonus() async {
    final userId = supabase.auth.currentUser!.id;
    final response  = await supabase
        .from('bonus')
        .select('*, profiles(*)').eq('profiles', userId);
    //print('object');
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Bonus(
        id: data['id'],
        number: data['number'],
        profiles: Profiles.fromJson(data['profiles']),
        created_at: data['created_at'] ,
      ))
          .toList();
      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }
  //add Bonus

  Future<List<Bonus>> getaddBonus(idUser, bonus) async {
    //final response = await Supabase.client.from('post').select().execute();
    final response =
    await supabase.from('bonus').insert({
      'number': bonus,
      'profiles': idUser,
    });
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Bonus(
        id: data['id'],
        number: data['number'],
        profiles: Profiles.fromJson(data['profiles']),
        created_at: data['created_at'] ,
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }
  //update Bonus
  Future<List<Bonus>> getupdateBonus(idUser, bonus) async {
    //final response = await Supabase.client.from('post').select().execute();
    final response =
    await supabase.from('bonus').update({
      'number': bonus,
    }).eq("profiles", idUser);
    print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Bonus(
        id: data['id'],
        number: data['number'],
        profiles: Profiles.fromJson(data['profiles']),
        created_at: data['created_at'] ,
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }


/*
  Future<List<Profiles>> getAllUser(String usersearch) async {
    //final response = await Supabase.client.from('post').select().execute();
    final response = await supabase.from(tableName)
        .select('fname, lname')
        .textSearch('fts', "$usersearch:*")
        .limit(100);
    //print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Profiles(
        id: data['id'].toString(),
        username: data['video_url'].toString(),
        avatar_url: data['description'].toString(),
        numero: data['numero'],
        email: data['email'].toString(),
        abonne: data['abonne'],
        NBabonne: data['NBabonne'],
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }


  Future<List<Profiles>> getAllUserALL() async {
    //final response = await Supabase.client.from('post').select().execute();
    final response = await supabase.from('profiles')
        .select()
        .limit(10);
    //print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Profiles(
        id: data['id'].toString(),
        username: data['username'].toString(),
        avatar_url: data['avatar_url'].toString(),
        numero: data['numero'],
        email: data['email'].toString(),
        abonne: data['abonne'],
        NBabonne: data['NBabonne'],
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }




  Future<List<Video>> getAllVideo() async {
    //final response = await Supabase.client.from('post').select().execute();
    final response = await supabase.from('post')
        .select()
        .limit(10);
    //print(response);

    if (response != null) {
      final notes = (response)
          .map((data) => Video(
        id: data['id'].toString(),
        description: data['description'].toString(),
        likes: data['likes'].toString(),
        video_url: data['video_url'],
        profiles: Profiles.fromJson(data['profiles']),
      ))
          .toList();

      return notes;
    } else {
      throw Exception('Failed to retrieve notes: ${response}');
    }
  }
*/

}