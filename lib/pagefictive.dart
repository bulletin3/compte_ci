import 'package:flutter/material.dart';

import '../Home/SousHome/UserHomePage.dart';
import '../Home/SousHome/UserInboxPage.dart';
import '../Home/SousHome/UserPlusPage.dart';
import '../Home/SousHome/UserProfilePage.dart';
import '../Home/SousHome/UserSearchPage.dart';

class HomePage1 extends StatefulWidget {
  HomePage1({super.key});

  @override
  State<HomePage1> createState() => _HomePage1State();
}

class _HomePage1State extends State<HomePage1> {
  //final user = FirebaseAuth.instance.currentUser!;
  void signUserOut() {
    //FirebaseAuth.instance.signOut();
  }
// bottom nav bar
  int? _selectedIndex = 3;
  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index!;
    });
  }

  final List<Widget> _pages = [
    UserHomePage(),
    UserSearchPage(),
    UserPlusPage(),
    UserInboxPage(),
    UserProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: _pages[_selectedIndex!],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex!,
        onTap: _navigateBottomBar,
        fixedColor: Theme.of(context).colorScheme.onBackground,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.install_desktop), label: 'Instagram'),
          BottomNavigationBarItem(icon: Icon(Icons.tiktok), label: 'TikTok'),
          BottomNavigationBarItem(icon: Icon(Icons.facebook), label: 'Facebook'),
          BottomNavigationBarItem(
              icon: Icon(Icons.monetization_on_outlined), label: 'Inbox'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
        ],
      ),
    );
  }
}
