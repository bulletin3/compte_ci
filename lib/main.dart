import 'dart:async';

import 'package:compte/pages/auth_page.dart';
import 'package:compte/pages/home_page.dart';
import 'package:compte/pages/login_page.dart';
import 'package:compte/repo/RepoUser.dart';
import 'package:compte/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:compte/verouu.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'Home/index.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  OneSignal.shared.setAppId("1e17c6fa-fc25-4e63-b9ae-a02b2d317a03");
  OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) =>
  {
    print("accepted: $accepted")
  }
  );

  await Supabase.initialize(
      url: 'https://xpwpjloprstesugfazkq.supabase.co',
      anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inhwd3BqbG9wcnN0ZXN1Z2ZhemtxIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM5ODgyMDUsImV4cCI6MjAyOTU2NDIwNX0.SQ2JwD_JNWG7b5Kd2I1rvQLNyd_0TeD7Pg6gNE0r91I'
  );



  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;

// Get a reference your Supabase client

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  String _debugLabelString = "";
  String? _emailAddress;
  String? _smsNumber;
  String? _externalUserId;
  String? _language;
  bool _enableConsentButton = false;

  // CHANGE THIS parameter to true if you want to test GDPR privacy consent
  bool _requireConsent = false;
  //




  //






  @override
  void initState() {
    //_redirect();
   /* supabase.auth.onAuthStateChange.listen((event) async {
      if(event.event == AuthChangeEvent.signedIn){
        await FirebaseMessaging.instance.requestPermission();

        await FirebaseMessaging.instance.getAPNSToken();
        final  fcmToken = await FirebaseMessaging.instance.getToken();
        if(fcmToken != null) {
          _setFcmToken(fcmToken);
        }
      }
    });
    FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) async {
      await _setFcmToken(fcmToken);
    });*/
    super.initState();
  }

  Future<void> _setFcmToken(String fctToken) async {
    final userId = supabase.auth.currentUser!.id;
    await supabase.from('profiles').upsert(
      {
        'id':userId,
        'fcm_token': fctToken
      }
    );
  }


  Future<void> _redirect() async
  {
    await Future.delayed(Duration.zero);
    final session = supabase.auth.currentSession;
    print(session);
    print('vir vour');
    if(session != null){
      Timer(Duration(seconds: 2), () {
        Get.to(HomePage());
      } );
    }
    else
    {
      Timer(Duration(seconds: 2), () {
        Get.off(Auth());
      } );
    }
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme:lightTheme ,
      darkTheme: dartTheme,
      //home:  Auth(),
      //backgroundColor: Theme.of(context).colorScheme.background,
      home: MyApp3(),
    );
  }
}
