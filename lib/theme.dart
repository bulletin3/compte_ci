import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
     // colorSchemeSeed: Colors.black,
      colorScheme: const ColorScheme.light(
    background: Colors.white,
        primary: Colors.white
)
);
ThemeData dartTheme = ThemeData(
    brightness: Brightness.dark,
    //colorSchemeSeed: Colors.white,
    colorScheme: ColorScheme.dark(
        background: Colors.grey.shade900,
      primary: Colors.grey.shade900
    )
);
