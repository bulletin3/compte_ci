class Typeprice {
  late final int id;
  late final String? nom;

  Typeprice({this.nom,required this.id});

  factory Typeprice.fromJson(Map<String, dynamic> json) => Typeprice(
    nom: json["nom"].toString() ,
    id: json["id"] ,
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'nom': nom,
  };
}