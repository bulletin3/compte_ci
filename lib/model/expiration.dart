import 'package:compte/model/type.dart';
import 'package:compte/model/typeprice.dart';

class Expiration {
  late final int? id;
  late final String? Date;
  late final String? Heure;
  late final String? created_at;

  Expiration({ this.created_at,this.id, this.Date, this.Heure});

  factory Expiration.fromJson(Map<String, dynamic> json) => Expiration(
    //type: Type.fromJson(json["type"]) ,
    id: json["id"],
    Date: json['Date'],
    Heure: json['Heure'],
    created_at: json["created_at"] ,
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'Date':Date,
    'Heure': Heure,
    'created_at': created_at,
  };
}