import 'package:compte/model/type.dart';
import 'package:compte/model/typeprice.dart';

class Likes {
  late final int? id;
  late final String? content;
  final Type? type;
  final bool? age;
  final Typeprice? typeprice;
  late final String? username;
  late final String? created_at;

  Likes({ this.age,this.type,this.username,this.created_at, this.content,  this.id, this.typeprice});

  factory Likes.fromJson(Map<String, dynamic> json) => Likes(
    //type: Type.fromJson(json["type"]) ,
    id: json["id"],
    age: json["age"],
    created_at: json["created_at"] ,
    username: json["username"].toString(),
    //type: Type.fromJson(json["type"]),
    content: json["content"].toString(),
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'typeprice': typeprice,
    'created_at': created_at,
    'age': age,
    //'profiles': Profiles,
    'username': username,
    'content': content,
    'type': Type,
  };
}