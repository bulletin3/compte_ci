import 'package:compte/model/pay.dart';
import 'package:compte/model/type.dart';
import 'package:compte/model/user.dart';

class Bonus {
  late final int? id;
  late final int? number;
  late final Profiles? profiles;
  late final String? created_at;

  Bonus({ this.profiles,this.created_at,this.number, this.id});

  factory Bonus.fromJson(Map<String, dynamic> json) => Bonus(
    profiles: Profiles.fromJson(json["profiles"]),
    id: json["id"] ,
    created_at: json["created_at"] ,
    number: json["number"],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'created_at': created_at,
    'number': number,
    'profiles': Profiles,
  };
}