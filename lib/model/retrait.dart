import 'package:compte/model/user.dart';

class Retrait {
  late final int? id;
  late final int? montant;
  late final int? numero;
  late final bool? payer;
  final Profiles? profil;
  late final String? mode;

  Retrait({this.montant,this.payer, this.profil, this.numero ,this.id, this.mode});

  factory Retrait.fromJson(Map<String, dynamic> json) => Retrait(
    montant: json["montant"],
    payer: json["payer"],
    numero: json["numero"],
    mode: json["mode"],
    id: json["id"] ,
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'numero': numero,
    'montant':montant,
    'payer': payer,
    'mode': mode
  };
}