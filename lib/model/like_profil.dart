import 'package:compte/model/pay.dart';
import 'package:compte/model/type.dart';
import 'package:compte/model/user.dart';

class Like_profil {
  late final int? id;
  late final bool activer;
  final Profiles? profiles;
  final Likes? likes;
  late final bool payer;
  late final String? created_at;

  Like_profil({ required this.activer,this.profiles,this.created_at, this.likes,  required this.payer, this.id});

  factory Like_profil.fromJson(Map<String, dynamic> json) => Like_profil(
    likes: Likes.fromJson(json["likes"]),
    profiles: Profiles.fromJson(json["profiles"]),
    id: json["id"] ,
    created_at: json["created_at"] ,
    activer: json["activer"],
    payer: json["payer"],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'created_at': created_at,
    //'profiles': Profiles,
    'payer': payer,
    'activer': activer,
    'profiles': Profiles,
    'likes': Likes
  };
}