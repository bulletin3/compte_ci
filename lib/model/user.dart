  class Profiles {
   late final String? id;
   late final String? username;
  late final String? email;
   late final String? avatar_url;
   late final int? numero;
   late final bool abonne;
   late final int? solde;
   late final String? nomUserTiktok;
   late final String? nomUserInstagram;
   late final String? nomUserFacebook;
   late final DateTime? dateAni;
   late final String? centre;
   late final int? age;
   late final int? numeroAboone;

   Profiles({this.centre ,this.age, this.dateAni,this.solde,this.numeroAboone ,this.nomUserTiktok, this.nomUserFacebook, this.nomUserInstagram,this.email,required this.abonne, this.avatar_url, this.numero, this.username, this.id});

  factory Profiles.fromJson(Map<String, dynamic> json) => Profiles(
    email: json["email"] ,
    dateAni: json["dateAni"] ,
    age: json["age"] ,
    numeroAboone: json["numeroAboone"] ,
    id: json["id"] ,
    solde: json["solde"] ,
    centre: json["centre"] ,
    avatar_url: json["avatar_url"] ,
    numero: json["numero"],
    username: json["username"],
    abonne: json["abonne"],
    nomUserTiktok: json["nomUserTiktok"],
    nomUserInstagram: json["nomUserInstagram"],
    nomUserFacebook: json["nomUserFacebook"],
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'age': age,
    'dateAni': dateAni,
    'solde': solde,
    'email': email,
    'avatar_url': avatar_url,
    'centre': centre,
    'numero': numero,
    'username': username,
    'numeroAboone': numeroAboone,
    'abonne': abonne,
    'nomUserTiktok': nomUserTiktok,
    'nomUserInstagram': nomUserInstagram,
    'nomUserFacebook': nomUserFacebook,
  };
  }