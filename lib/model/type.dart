class Type {
  late final int id;
  late final String? nom;

  Type({this.nom,required this.id});

  factory Type.fromJson(Map<String, dynamic> json) => Type(
    nom: json["nom"].toString() ,
    id: json["id"] ,
  );

  Map<String, dynamic> toJson() => {
    'id': id,
    'nom': nom,
  };
} 