import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Intro2 extends StatefulWidget {
  const Intro2({super.key});

  @override
  State<Intro2> createState() => _Intro2State();
}

class _Intro2State extends State<Intro2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset("lib/images/valider.json"),
            Text('Abonnez-vous et likez les vidéos'),
            Text("Pour avoir beaucoup plus d'argent"),
            Text("Avec seulement le téléphone"),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Nb:"),
                Text('Vous ne payez rien ', style: TextStyle(
                    color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20
                ),)
              ],
            ),
          ],
        ),
      ),
    );
  }
}

