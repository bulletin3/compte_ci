import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Intro3 extends StatefulWidget {
  const Intro3({super.key});

  @override
  State<Intro3> createState() => _Intro3State();
}

class _Intro3State extends State<Intro3> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Lottie.asset("lib/images/Depot.json"),
            Text('Consulter Votre Solde'),
            Text("A partir de 1000 fr "),
            Text("Faites vos retraits "),
            Text("Faites vos retraits sur tous les réseaux "),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Nb:"),
                Text('Vous ne payez rien', style: TextStyle(
                    color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20
                ),)
              ],
            ),
          ],
        ),
      ),
    );
  }
}

