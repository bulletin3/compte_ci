import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Intro1 extends StatefulWidget {
  const Intro1({super.key});

  @override
  State<Intro1> createState() => _Intro1State();
}

class _Intro1State extends State<Intro1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
       child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         children: [
           Lottie.asset("lib/images/list.json"),
           Text('Cliquer sur la tâche à effectuer (TikTok, Instagram,'),
           Text("Facebook)"),
           Text("Choisissez le réseau social"),
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               Text("Nb:"),
               Text('Vous ne payez rien ', style: TextStyle(
                 color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20
               ),)
             ],
           ),
         ],
       ),
      ),
    );
  }
}
