import 'package:flutter/material.dart';

import '../Home/SousHome/UserHomePage.dart';
import '../Home/SousHome/UserInboxPage.dart';
import '../Home/SousHome/UserPlusPage.dart';
import '../Home/SousHome/UserProfilePage.dart';
import '../Home/SousHome/UserSearchPage.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //final user = FirebaseAuth.instance.currentUser!;
  void signUserOut() {
    //FirebaseAuth.instance.signOut();
  }
// bottom nav bar
  int? _selectedIndex = 0;
  void _navigateBottomBar(int index) {
    setState(() {
      _selectedIndex = index!;
    });
  }

  final List<Widget> _pages = [
    UserHomePage(),
    UserSearchPage(),
    UserPlusPage(),
    UserInboxPage(),
    UserProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pages[_selectedIndex!],
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Theme.of(context).colorScheme.onBackground,
        currentIndex: _selectedIndex!,
        onTap: _navigateBottomBar,
        type: BottomNavigationBarType.fixed,
        items:  [
          BottomNavigationBarItem(icon: Icon(Icons.install_desktop), label: 'Instagram'),
          BottomNavigationBarItem(icon: Icon(Icons.tiktok), label: 'TikTok'),
          BottomNavigationBarItem(icon: Icon(Icons.facebook), label: 'Facebook'),
          BottomNavigationBarItem(icon: Icon(Icons.monetization_on_outlined), label: 'Solde'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
        ],
      ),
    );
  }
}
