import 'package:compte/pages/home_page.dart';
import 'package:compte/pages/intro/intro.dart';
import 'package:compte/pages/intro/intro2.dart';
import 'package:compte/pages/intro/intro3.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../Home/index.dart';

class Auth extends StatefulWidget {
  const Auth({super.key});

  @override
  State<Auth> createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  final PageController _controller = PageController();
  bool onlastPage = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: _controller,
            onPageChanged: (index) {
              setState(() {
                onlastPage = (index ==2);
              });
            },
            children: [
              Intro1(),
              Intro2(),
              Intro3(),
            ],
          ),
          Container(
              alignment: Alignment(0, 0.85),

              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                     onTap: () {
                       _controller.jumpToPage(2);
                     },
                      child: Text('Passer')
                  ),
                  SmoothPageIndicator(controller: _controller, count: 3),

                  onlastPage?
                  GestureDetector(
                    onTap: () {
                      Navigator.push(context,
                      MaterialPageRoute(builder: (context)
                      {
                        return Index();
                      }
                      )
                      );
                    },
                      child: Text('Entrer')
                  ):
                  GestureDetector(
                      onTap: () {
                        _controller.nextPage(duration: Duration(microseconds: 500), curve: Curves.ease);
                      },
                      child: Text('Next')
                  )
                ],
              )
          )
        ],
      ),
    );
  }
}
