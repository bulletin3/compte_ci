import 'package:compte/pages/auth_page.dart';
import 'package:compte/pages/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';

class MyApp3 extends StatefulWidget {

  @override
  State<MyApp3> createState() => _MyApp3State();
}

class _MyApp3State extends State<MyApp3> {
  late final LocalAuthentication auth;
  bool _supported = false;
  LocalAuthentication authentication = LocalAuthentication();


  @override
  void initState(){
    super.initState();
    auth = LocalAuthentication();
    auth.isDeviceSupported().then((bool isSupported) => setState(() {
      _supported = isSupported;
    }),
    );

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      ), 
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:<Widget> [
            if(_supported)
                Image.asset('assets/logo.png', width: 200,height: 500,)
            else
              const Text('ooojoijij'),

            ElevatedButton(onPressed: _getOk1, child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.fingerprint, color: Colors.blue,),
                Text('Deverouller', style: TextStyle(color: Colors.blue),),
              ],
            )
            )
          ],
        ),
      )
    );
  }

  Future<void> _getOk1() async {
    try{
      bool authent = await auth.authenticate(localizedReason: 'Deverrouiller votre application', );

          print("auuuuttt:: $authent");
          if(authent == true) {
            Get.offAll(Auth());
          }
          else{
            print('iijij');
          }



    } on PlatformException catch(e){

    }
  }
  Future<void> _getOk() async {
    List<BiometricType> Biometric = await auth.getAvailableBiometrics();

    print('list: $Biometric');

    if(!mounted)
      {
        return;
      }


  }
}

class SecretPage extends StatelessWidget {
  const SecretPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Secret Page"),
      ),
      body: const Center(child: Text("Hooray, Your secret is still safe")),
    );
  }
}