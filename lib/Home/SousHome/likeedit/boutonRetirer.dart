import 'package:flutter/material.dart';

class MyButtonRetirer extends StatelessWidget {
  final Function()? onTap;
  final Function()? onTap1;
  final int? Verif;

  const MyButtonRetirer({super.key, required this.onTap, this.Verif, this.onTap1});

  @override
  Widget build(BuildContext context) {
    return Verif! >= 1000 ? GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.symmetric(horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(8),
        ),
        child: const Center(
          child: Text(
            "Retier 10min",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
      ),
    ):
    GestureDetector(
      onTap: onTap1,
      child: Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.symmetric(horizontal: 25),
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(8),
        ),
        child: const Center(
          child: Text(
            "Avoir 1000fr",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }
}
