import 'package:compte/model/bonus.dart';
import 'package:compte/model/pay.dart';
import 'package:compte/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/link.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../model/notes.dart';
import '../../main.dart';
import '../../model/like_profil.dart';
import '../../repo/RepoUser.dart';

class EditScreen1 extends StatefulWidget {
  final Likes? note;
  const EditScreen1({super.key, this.note});

  @override
  State<EditScreen1> createState() => _EditScreen1State();
}

class _EditScreen1State extends State<EditScreen1> {
  Future<void>? _launched;
  late List<Profiles> soldeAugu = [];
  var soledPlus ;
  var soledPlusAbonne;
  late List<Bonus> Bonuss = [];
  var angebonus ;
  var taille;
  var taillebonnus ;
  late List<Like_profil> verifiAffiche = [];
  TextEditingController _titleController = TextEditingController();
  TextEditingController _contentController = TextEditingController();
  late String urls ;
  var _url;
  var _userId = supabase.auth.currentSession?.user.id;

  Future<void> _getBonus() async {
    final videoUser = await _noteRepository.getBonus();
    print(videoUser);
    setState(() {
      Bonuss = videoUser!;
      angebonus = Bonuss[0]!.number!;
      taille = Bonuss!.length!;
      //print(filteredNotes);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    if (widget.note != null) {
      _titleController = TextEditingController(text: widget.note!.username);
      _contentController = TextEditingController(text: widget.note!.content);
      urls=  widget.note!.content!;

    }
    _getBonus();
    _getUseSoldeProfil();
    //_getAutoLikes(_userId, widget.note!.id);
    _getAutoActive(_userId, widget.note!.id);

    super.initState();
  }

  Future<void> _launchInBrowser(Uri url) async {
    if (!await launchUrl(
      url,
      mode: LaunchMode.externalApplication,
    )) {
      throw Exception('Could not launch $url');
    }
  }



  final NoteRepository _noteRepository = NoteRepository();

  Future<void> _getAutoLikes(userId, like_id) async {
    final videoUser1 = await _noteRepository.getAutoLike(userId, like_id);



  }

  Future<void> _getUseSoldeProfil() async {
    final videoUser1 = await _noteRepository.getUserSolde();
    setState(() {
      soldeAugu = videoUser1;
      soledPlus = (videoUser1[0].solde! + 50)!;
      soledPlusAbonne = (videoUser1[0].solde! + 100)!;
      print('ange');
      print(soledPlus);
    });

  }

  Future<void> _getSoldeAu(userId, solde) async {
    final videoUser1 = await _noteRepository.getSoldeLike(userId, solde);
  }

  Future<void> _getaddBonus(userId) async {
    final videoUser1 = await _noteRepository.getaddBonus(userId, 1);
  }

  Future<void> _getuopadateBonus(userId) async {
    final videoUser1 = await _noteRepository.getupdateBonus(userId,  angebonus + 1);

  }


  Future<void> _getAutoActive(userId, like_id) async {
    final videoUser2 = await _noteRepository.getVoirActive(userId, like_id);
    setState(() {
      verifiAffiche = videoUser2;
      //print(verifiAffiche.length);
    });

  }


  @override
  Widget build(BuildContext context) {
    final Uri toLaunch =
    Uri.parse('${urls}');
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      body: Padding(
        padding: const EdgeInsets.fromLTRB(16, 40, 16, 0),
        child: Column(children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  padding: const EdgeInsets.all(0),
                  icon: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                        color: Colors.grey.shade800.withOpacity(.8),
                        borderRadius: BorderRadius.circular(10)),
                    child: const Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.white,
                    ),
                  ))
            ],
          ),
          Expanded(
              child: ListView(
                children: [
                  Text(' ${
                      _titleController.text.trim()
                  }',
                    style: const TextStyle(color: Colors.white, fontSize: 30),
                  ),
                  TextButton(
                    child:  Text('${_contentController.text.trim()}',  style: const TextStyle(
                      color: Colors.white,
                    ),),
                    onPressed: () => setState(() async{
                     // final reee =await _getAutoActive(_userId, widget.note?.id);
                      if(verifiAffiche.length == 0)
                        {
                          if(widget.note?.typeprice?.nom == 'abonne')
                            {
                              _getSoldeAu(_userId, soledPlusAbonne);
                              _getAutoLikes(_userId, widget.note!.id);
                              _launched = _launchInBrowser(toLaunch);
                              if(taille != 1)
                                {
                                  _getaddBonus(_userId);
                                }
                              else{
                                _getuopadateBonus(_userId);
                              }
                            }else{
                            if(taille != 1)
                            {
                              _getaddBonus(_userId);
                            }else{
                              _getuopadateBonus(_userId);
                            }
                            _getSoldeAu(_userId, soledPlus);
                            _getAutoLikes(_userId, widget.note!.id);
                            _launched = _launchInBrowser(toLaunch);
                          }
                        }else{
                        final result = await confirmActiver(context);
                        if (result != null && result) {
                          // deleteNote(index);
                        }
                      }
                      //verifiAffiche.length

                    }),
                  ),
                ],
              )),
        ]),
      ),
      /* floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pop(
              context, [_titleController.text, _contentController.text]);
        },
        elevation: 10,
        backgroundColor: Colors.grey.shade800,
        child: const Icon(Icons.save),
      ),*/
    );
  }


  Future<dynamic> confirmActiver(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'element en cour de verifucation si oui patienter 24h pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }



}