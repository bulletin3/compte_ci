import 'package:compte/Home/SousHome/addProffil.dart';
import 'package:compte/model/bonus.dart';
import 'package:compte/model/expiration.dart';
import 'package:compte/model/like_profil.dart';
import 'package:compte/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import '../../provider/provider.dart';
import '../../repo/RepoUser.dart';

class UserProfilePage extends StatefulWidget {
  @override
  State<UserProfilePage> createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  var _idUV;

  //final NoteRepository _noteRepository = NoteRepository();

  var ange = "";

  var emailProfil;
  String? _avatar;
  var numeroProfil;
  //late List<Video> _videoUserId = [];

  @override
  void initState() {
    _redirect();
    _getProfil();
    _getLikeAll();
    _getExpire();
    _getBonus();
    //_getVideoUser();
    super.initState();
  }

  var _userId = supabase.auth.currentSession?.user.id;
  final NoteRepository _noteRepository = NoteRepository();
  late List<Like_profil> filteredNotes = [];
  late List<Expiration> expiration = [];

  Future<void> _getLikeAll() async {
    final videoUser = await _noteRepository.getLikeProfile();
    print(videoUser);
    setState(() {
      filteredNotes = videoUser;
      //print(filteredNotes);
    });
  }

  Future<void> _getExpire() async {
    final videoUser = await _noteRepository.getExpire();
    print(videoUser);
    setState(() {
      expiration = videoUser;
      ange = expiration[0].Date!;
      //print(filteredNotes);
    });
  }

  late List<Bonus> Bonuss = [];
  var angebonus = 0;
  var taille = 0;

  Future<void> _getBonus() async {
    final videoUser = await _noteRepository.getBonus();
    print(videoUser);
    setState(() {
      Bonuss = videoUser;
      angebonus = Bonuss[0]!.number!;
      taille = Bonuss.length!;
      //print(filteredNotes);
    });
  }

  Future<void> onRefresh() async {
    await Future.delayed(Duration(seconds: 2));
    if (!mounted) return;
    _redirect();
    _getProfil();
    _getLikeAll();
    _getExpire();
    _getBonus();
  }

  Future<void> _redirect() async {
    await Future.delayed(Duration.zero);
    final session = supabase.auth.currentSession;
    //print(session);
    setState(() {
      emailProfil = session?.user.email;
      numeroProfil = session?.user.userMetadata!['username'];
    });
    print(emailProfil);
  }

  Future<void> _getProfil() async {
    final userId = supabase.auth.currentUser!.id;
    final data =
        await supabase.from('profiles').select().eq('id', userId).single();
    setState(() {
      _avatar = data['avatar_url'];
    });
  }

  Future<void> _deconnexion() async {
    final userId = supabase.auth.currentUser!.id;
    final data = await supabase.auth.signOut();
    Get.off(LoginPage());
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: Container(
        child: DefaultTabController(
          length: 1,
          child: Scaffold(
            appBar: AppBar(
              title: Text('${numeroProfil}',
              ),
              backgroundColor: Colors.transparent,
              elevation: 0,
              leading: IconButton(
                onPressed: () async {
                  await _deconnexion();
                },
                icon: Icon(Icons.exit_to_app),
              ),
              actions: [
                Padding(
                  padding: const EdgeInsets.only(right: 12.0),
                  child: IconButton(
                    onPressed: ()  {
                      Get.to(Profiladd());
                    },
                    icon: Icon(Icons.add_circle_outline_rounded),
                  ),
                )
              ],
            ),
            backgroundColor: Theme.of(context).colorScheme.background,
            body: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    '${numeroProfil}',
                    style: TextStyle( fontSize: 20),
                  ),
                ),

                // number of following, followers, likes
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        alignment: Alignment.center,
                        child: Column(
                          children: [
                            taille != 1
                                ? Text(
                                    '0',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24),
                                  )
                                : Text(
                                    '${angebonus}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24),
                                  ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              '  (Like et Abonnement) ',
                              style:
                                  TextStyle( fontSize: 15),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 15),

                // buttons -> edit profile, insta links, bookmark
                /* Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
                      child: Text('Edit profile',
                          style: TextStyle(color: Colors.black, fontSize: 20)),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.grey.shade300),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                  ],
                ),*/
                SizedBox(height: 15),

                // bio
                Text(
                  'Historique de retrait sera effacé ',
                ),
                Text(
                  '${ange}',
                  style: TextStyle(color: Colors.red[700]),
                ),

                // default tab controller

                TabBar(
                  tabs: [
                    Tab(
                      icon: Icon(Icons.grid_3x3, color: Colors.black),
                    ),
                    /*Tab(
                      icon: Icon(Icons.favorite, color: Colors.black),
                    ),
                    Tab(
                      icon: Icon(Icons.lock_outline_rounded, color: Colors.black),
                    ),*/
                  ],
                ),
                Expanded(
                    child: ListView.builder(
                  padding: const EdgeInsets.only(top: 30),
                  itemCount: filteredNotes.length,
                  itemBuilder: (context, index) {
                    final _Like = filteredNotes![index];
                    return Card(
                      margin: const EdgeInsets.only(bottom: 20),
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ListTile(
                          onTap: () async {
                            final result = await confirmDialog(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }
                          },
                          title: RichText(
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                text: '${_Like.likes?.username} \n',
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    height: 1.8),
                                children: [
                                  TextSpan(
                                    text: _Like.likes?.content,
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14,
                                        height: 2),
                                  )
                                ]),
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Edited: ${
                              //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
                              _Like.likes?.created_at}',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontStyle: FontStyle.italic,
                                  color: Colors.grey.shade800),
                            ),
                          ),

                          /* trailing:   Column(
                                  children: [
                                    IconButton(
                                      onPressed: () async {
                                        final result = await confirmDialog(context);
                                        if (result != null && result) {
                                          // deleteNote(index);
                                        }
                                      },
                                      icon: const Icon(
                                        Icons.pending,
                                      ),
                                    )
                                  ],
                                )*/
                        ),
                      ),
                    );
                  },
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<dynamic> confirmDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              "Deja Payer",
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'OK',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }
}
