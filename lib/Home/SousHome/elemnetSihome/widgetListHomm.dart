import 'package:compte/model/pay.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../main.dart';
import '../../../model/like_profil.dart';
import '../../../repo/RepoUser.dart';
import '../lieedit.dart';

class ListHome extends StatefulWidget {
   ListHome({super.key, required this.Index, required this.likes, required this.file});
   var Index; Likes likes; late List<Likes> file = [];
  @override
  State<ListHome> createState() => _ListHomeState();
}

class _ListHomeState extends State<ListHome> {
  late List<Like_profil> essai = [];
  var _userId = supabase.auth.currentSession?.user.id;
  final NoteRepository _noteRepository = NoteRepository();



  @override
  Widget build(BuildContext context) {
    return essai[widget.Index].activer ==false?

      ListTile(
      onTap: () async  {
        await confirmActiver(context);
      },
      title: RichText(
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        text: TextSpan(
            text: '${essai[widget.Index].likes?.username} \n',
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                height: 1.5),
            children: [
              TextSpan(
                text: essai[widget.Index].likes?.content,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    height: 1.5),
              )
            ]),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          'Edited: ${
          //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
              essai[widget.Index].likes?.created_at
          }',
          style: TextStyle(
              fontSize: 10,
              fontStyle: FontStyle.italic,
              color: Colors.grey.shade800),
        ),
      ),
      trailing: Column(
        children: [
          IconButton(
            onPressed: () async {
              final result = await confirmDialog(context);
              if (result != null && result) {
                // deleteNote(index);
              }
            },
            icon: const Icon(
              Icons.pending,
            ),
          ),
        ],
      ),
    ):
    ListTile(
      onTap: () async {
        final result = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  EditScreen1(note: widget.likes)),
        );
        if (result != null) {
          setState(() {
            int originalIndex = widget.file.indexOf(widget.likes);

            widget.file[originalIndex] = Likes(
              id: widget.file[originalIndex].id,
              username: result[0],
              content: result[1],
              created_at: DateTime.now() as String,
            );

            widget.file[widget.Index] = Likes(
              id: widget.file[originalIndex].id,
              username: result[0],
              content: result[1],
              created_at: DateTime.now() as String,
            );
          });
        }
      },
      title: RichText(
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        text: TextSpan(
            text: '${widget.likes?.username} \n',
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                height: 1.5),
            children: [
              TextSpan(
                text: widget.likes?.content,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    height: 1.5),
              )
            ]),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          'Edited: ${
          //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
              widget.likes?.type?.nom}',
          style: TextStyle(
              fontSize: 10,
              fontStyle: FontStyle.italic,
              color: Colors.grey.shade800),
        ),
      ),
      trailing: Column(
        children: [
          IconButton(
            onPressed: () async {
              final result = await confirmDialog(context);
              if (result != null && result) {
                // deleteNote(index);
              }
            },
            icon: const Icon(
              Icons.pending,
            ),
          ),
        ],
      ),
    );
  }












   Future<dynamic> confirmDialog(BuildContext context) {
     return showDialog(
         context: context,
         builder: (BuildContext context) {
           return AlertDialog(
             backgroundColor: Colors.grey.shade900,
             icon: const Icon(
               Icons.info,
               color: Colors.grey,
             ),
             title: const Text(
               'Vous avez appuyer? si oui patienter 24h pour etre credité',
               style: TextStyle(color: Colors.white),
             ),
             content: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: [
                   ElevatedButton(
                       onPressed: () {
                         Navigator.pop(context, true);
                       },
                       style: ElevatedButton.styleFrom(
                           backgroundColor: Colors.green),
                       child: const SizedBox(
                         width: 60,
                         child: Text(
                           'Yes',
                           textAlign: TextAlign.center,
                           style: TextStyle(color: Colors.white),
                         ),
                       )),
                   ElevatedButton(
                       onPressed: () {
                         Navigator.pop(context, false);
                       },
                       style:
                       ElevatedButton.styleFrom(backgroundColor: Colors.red),
                       child: const SizedBox(
                         width: 60,
                         child: Text(
                           'No',
                           textAlign: TextAlign.center,
                           style: TextStyle(color: Colors.white),
                         ),
                       )),
                 ]),
           );
         });
   }

   Future<dynamic> confirmActiver(BuildContext context) {
     return showDialog(
         context: context,
         builder: (BuildContext context) {
           return AlertDialog(
             backgroundColor: Colors.grey.shade900,
             icon: const Icon(
               Icons.info,
               color: Colors.grey,
             ),
             title: const Text(
               'element en cour de verifucation si oui patienter 24h pour etre credité',
               style: TextStyle(color: Colors.white),
             ),
             content: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceAround,
                 children: [
                   ElevatedButton(
                       onPressed: () {
                         Navigator.pop(context, false);
                       },
                       style:
                       ElevatedButton.styleFrom(backgroundColor: Colors.red),
                       child: const SizedBox(
                         width: 60,
                         child: Text(
                           'ok',
                           textAlign: TextAlign.center,
                           style: TextStyle(color: Colors.white),
                         ),
                       )),
                 ]),
           );
         });
   }
}
