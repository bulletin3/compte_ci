import 'package:compte/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:iconsax/iconsax.dart';
import 'package:animate_do/animate_do.dart';

import '../../components/my_textfield.dart';
import '../../main.dart';
import '../../model/retrait.dart';
import '../../repo/RepoUser.dart';
import 'likeedit/boutonRetirer.dart';

class UserInboxPage extends StatefulWidget {
  const UserInboxPage({super.key});

  @override
  State<UserInboxPage> createState() => _UserInboxPageState();
}

class _UserInboxPageState extends State<UserInboxPage>with TickerProviderStateMixin  {
  late final AnimationController _controller;
  final NoteRepository _noteRepository = NoteRepository();
  late List<Profiles> _UserSolde = [];
  late List<Retrait> retraitfield = [];
  late List<Retrait> Allretrait = [];
  var _userId = supabase.auth.currentSession?.user.id;
  late List<Profiles> soldeAugu = [];
  //late List<Retrait> retraitfield = [];
  var soledPlus;

  var rav ;

  @override
  void initState() {
    _getSoldeUser();
    _scrollController = ScrollController();
    _scrollController.addListener(_listenToScrollChange);
    _controller = AnimationController(vsync: this);
    _getUseSoldeProfil();
    getAllRetrait();
    super.initState();
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> _getSoldeUser() async {
    final session = supabase.auth.currentSession;

    final videoUser = await _noteRepository.getUserSolde();

    setState(() {
      _UserSolde = videoUser;
      print(_UserSolde);
    });
  }

  Future<void> _getUseSoldeProfil() async {
    final videoUser1 = await _noteRepository.getUserSolde();
    setState(() {
      soldeAugu = videoUser1;
      soledPlus = (videoUser1[0].solde! )!;
      print('ange');
      print(soledPlus);
    });

  }
  Future<void> _getSoldeAu(userId, solde) async {
    final videoUser1 = await _noteRepository.getSoldeLike(userId, solde);


  }

  Future<void> getAllRetrait() async {
    final videoUser1 = await _noteRepository.getRetraitAll();
    setState(() {
      Allretrait = videoUser1;
      rav = Allretrait.length;
    });
  }

  Future<void> getInserRetrait(montant , numero) async {
    final videoUser = await _noteRepository.getRetrait(montant , numero);

    print(videoUser);
    setState(() {
      retraitfield = videoUser;
      //print(filteredNotes);
    });
  }

  final TextEditingController numeroController = TextEditingController();
  final TextEditingController montantController = TextEditingController();

  late ScrollController _scrollController;
  bool _isScrolled = false;

  List<dynamic> _services = [
    ['Orange', Iconsax.export_1, Colors.blue],
    ['Mtn', Iconsax.import, Colors.pink],
    ['Moov', Iconsax.wallet_3, Colors.orange],
    ['Waave', Iconsax.more, Colors.green],
  ];

  List<dynamic> _transactions = [
    ['Amazon', 'https://img.icons8.com/color/2x/amazon.png', '6:25pm', '\$8.90'],
    ['Cash from ATM', 'https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/2x/external-atm-banking-and-finance-kiranshastry-lineal-color-kiranshastry.png', '5:50pm', '\$200.00'],
    ['Netflix', 'https://img.icons8.com/color-glass/2x/netflix.png', '2:22pm', '\$13.99'],
    ['Apple Store', 'https://img.icons8.com/color/2x/mac-os--v2.gif', '6:25pm', '\$4.99'],
    ['Cash from ATM', 'https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/2x/external-atm-banking-and-finance-kiranshastry-lineal-color-kiranshastry.png', '5:50pm', '\$200.00'],
    ['Netflix', 'https://img.icons8.com/color-glass/2x/netflix.png', '2:22pm', '\$13.99']
  ];


  void _listenToScrollChange() {
    if (_scrollController.offset >= 100.0) {
      setState(() {
        _isScrolled = true;
      });
    } else {
      setState(() {
        _isScrolled = false;
      });
    }
  }

  final _advancedDrawerController = AdvancedDrawerController();

  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          backgroundColor: Colors.grey.shade100,
          body: CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverAppBar(
                  expandedHeight: 250.0,
                  elevation: 0,
                  pinned: true,
                  stretch: true,
                  toolbarHeight: 80,
                  backgroundColor: Colors.white,
                  /* leading: IconButton(
                    color: Colors.black,
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: Icon(
                            value.visible ? Iconsax.close_square : Iconsax.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),*/
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                  ),
                  centerTitle: true,
                  title: AnimatedOpacity(
                    opacity: _isScrolled ? 1.0 : 0.0,
                    duration: const Duration(milliseconds: 500),
                    child: Column(
                      children: [
                        Text(
                          '\$ 1,840.00',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          width: 30,
                          height: 4,
                          decoration: BoxDecoration(
                            color: Colors.grey.shade800,
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.pin,
                    titlePadding: const EdgeInsets.only(left: 20, right: 20),
                    title: AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: _isScrolled ? 0.0 : 1.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FadeIn(
                            duration: const Duration(milliseconds: 500),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('\$', style: TextStyle(color: Colors.grey.shade800, fontSize: 22),),
                                SizedBox(width: 3,),
                                Text('1,840.00',
                                  style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 10,),
                          FadeIn(
                            duration: const Duration(milliseconds: 500),
                            child: MaterialButton(
                              height: 30,
                              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                              onPressed: () {},
                              /* child: Text('Add Money', style: TextStyle(color: Colors.black, fontSize: 10),),*/
                              color: Colors.transparent,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey.shade300, width: 1),
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            width: 30,
                            height: 3,
                            decoration: BoxDecoration(
                              color: Colors.grey.shade800,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          SizedBox(height: 8,),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverList(
                    delegate: SliverChildListDelegate([
                      SizedBox(height: 20,),
                      Container(
                        padding: EdgeInsets.only(top: 20),
                        height: 115,
                        width: double.infinity,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _services.length,
                          itemBuilder: (context, index) {
                            return FadeInDown(
                              duration: Duration(milliseconds: (index + 1) * 100),
                              child: AspectRatio(
                                aspectRatio: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    if (_services[index][0] == 'Transfer') {
                                      // Navigator.push(context, MaterialPageRoute(builder: (context) => ContactPage()));
                                    }
                                  },
                                  child: Column(
                                    children: [
                                      Container(
                                        width: 60,
                                        height: 60,
                                        decoration: BoxDecoration(
                                          color: Colors.grey.shade900,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Center(
                                          child: Icon(_services[index][1], color: Colors.white, size: 25,),
                                        ),
                                      ),
                                      SizedBox(height: 10,),
                                      Text(_services[index][0], style: TextStyle(color: Colors.grey.shade800, fontSize: 12),),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ])
                ),
                SliverFillRemaining(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 30),
                    child: Column(
                      children: [
                        FadeInDown(
                          duration: Duration(milliseconds: 500),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Today', style: TextStyle(color: Colors.grey.shade800, fontSize: 14, fontWeight: FontWeight.w600),),
                                SizedBox(width: 10,),
                                Text('\$ 1,840.00', style: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w700,)),
                              ]
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.only(top: 20),
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: _transactions.length,
                            itemBuilder: (context, index) {
                              return FadeInDown(
                                duration: Duration(milliseconds: 500),
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.shade200,
                                        blurRadius: 5,
                                        spreadRadius: 1,
                                        offset: Offset(0, 6),
                                      ),
                                    ],
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Image.network(_transactions[index][1], width: 50, height: 50,),
                                          SizedBox(width: 15,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(_transactions[index][0], style: TextStyle(color: Colors.grey.shade900, fontWeight: FontWeight.w500, fontSize: 14),),
                                              SizedBox(height: 5,),
                                              Text(_transactions[index][2], style: TextStyle(color: Colors.grey.shade500, fontSize: 12),),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Text(_transactions[index][3], style: TextStyle(color: Colors.grey.shade800, fontSize: 16, fontWeight: FontWeight.w700),),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ]
          )
      );
  }

  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
}

Widget myText(String txt) {
  double myHeight = 200;
  double myWidth = 200;
  return Padding(
    padding: EdgeInsets.symmetric(horizontal: myWidth * 0.03),
    child: Text(
      txt,
      style: TextStyle(
          fontSize: 27, color: Colors.white, fontWeight: FontWeight.normal),
    ),
  );
}

Widget myItemContiner(String title, String price) {
  double myHeight = 200;
  double myWidth = 200;
  return Container(
    padding: EdgeInsets.only(
        top: myHeight * 0.02,
        left: myWidth * 0.05,
        right: myWidth * 0.1,
        bottom: myHeight * 0.02),
    decoration: BoxDecoration(
        color: Colors.white, borderRadius: BorderRadius.circular(15)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
              color: Colors.grey, fontSize: 15, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: myHeight * 0.01,
        ),
        Row(
          children: [
            Text(
              '\F ',
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              price,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ],
        )
      ],
    ),
  );
}




///////////////////////////////////////


import 'package:compte/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../components/my_textfield.dart';
import '../../main.dart';
import '../../model/retrait.dart';
import '../../repo/RepoUser.dart';
import 'likeedit/boutonRetirer.dart';

class UserInboxPage extends StatefulWidget {
  const UserInboxPage({super.key});

  @override
  State<UserInboxPage> createState() => _UserInboxPageState();
}

class _UserInboxPageState extends State<UserInboxPage>with TickerProviderStateMixin  {
  late final AnimationController _controller;
  final NoteRepository _noteRepository = NoteRepository();
  late List<Profiles> _UserSolde = [];
  late List<Retrait> retraitfield = [];
  late List<Retrait> Allretrait = [];
  var _userId = supabase.auth.currentSession?.user.id;
  late List<Profiles> soldeAugu = [];
  //late List<Retrait> retraitfield = [];
  var soledPlus;

  var rav ;

  @override
  void initState() {
    _getSoldeUser();
    _controller = AnimationController(vsync: this);
    _getUseSoldeProfil();
    getAllRetrait();
    super.initState();
  }
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> _getSoldeUser() async {
    final session = supabase.auth.currentSession;

    final videoUser = await _noteRepository.getUserSolde();

    setState(() {
      _UserSolde = videoUser;
      print(_UserSolde);
    });
  }

  Future<void> _getUseSoldeProfil() async {
    final videoUser1 = await _noteRepository.getUserSolde();
    setState(() {
      soldeAugu = videoUser1;
      soledPlus = (videoUser1[0].solde! )!;
      print('ange');
      print(soledPlus);
    });

  }
  Future<void> _getSoldeAu(userId, solde) async {
    final videoUser1 = await _noteRepository.getSoldeLike(userId, solde);


  }

  Future<void> getAllRetrait() async {
    final videoUser1 = await _noteRepository.getRetraitAll();
    setState(() {
      Allretrait = videoUser1;
      rav = Allretrait.length;
    });
  }

  Future<void> getInserRetrait(montant , numero) async {
    final videoUser = await _noteRepository.getRetrait(montant , numero);

    print(videoUser);
    setState(() {
      retraitfield = videoUser;
      //print(filteredNotes);
    });
  }

  final TextEditingController numeroController = TextEditingController();
  final TextEditingController montantController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Scaffold(
          backgroundColor: Colors.grey.shade100,
          body: Container(
              height: myHeight,
              width: myWidth,
              child: Column(
                children: [
                  SizedBox(
                    height: myHeight * 0.04,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: myWidth * 0.05),
                    child: Container(
                      height: myHeight * 0.23,
                      // width: myWidth * 0.9,
                      decoration: BoxDecoration(
                          color: Color(0xff7847FC),
                          borderRadius: BorderRadius.circular(30)),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: myWidth * 0.05,
                                vertical: myHeight * 0.014),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  'lib/images/1.png',
                                  height: myHeight * 0.04,
                                  color: Colors.white,
                                ),
                                Container(
                                  // height: myHeight * 0.03,
                                  // width: myWidth * 0.1,
                                  padding: EdgeInsets.symmetric(
                                      horizontal: myWidth * 0.02,
                                      vertical: myHeight * 0.005),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white.withOpacity(0.5)),
                                  child: Center(
                                      child: Text(
                                        '02/04',
                                        style: TextStyle(color: Colors.white),
                                      )),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                            EdgeInsets.symmetric(vertical: myHeight * 0.01),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                myText('6104'),
                                myText('3389'),
                                myText('3256'),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: myWidth * 0.05,
                                vertical: myHeight * 0.01),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  '${_UserSolde[0]!.username}',
                                  style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal),
                                ),
                                const Text(
                                  'CVV2: 452',
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: myWidth * 0.08, vertical: myHeight * 0.03),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        myItemContiner('Montant Total', '${_UserSolde[0].solde}'),
                        rav == 0 ?
                        MyButtonRetirer(
                          onTap: () async {
                            final result = await confirmDialog(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }
                          },
                          Verif: _UserSolde[0].solde,
                          onTap1: () async {
                            final result = await confirmDialog1(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }
                          },
                        ):
                        MyButtonRetirer(
                          onTap: () async {
                            final result = await erreur(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }
                          },
                          Verif: _UserSolde[0].solde,
                          onTap1: () async {
                            final result = await erreur(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }
                          },
                        )
                        //myItemContiner('Montant retirable', '90.000'),
                      ],
                    ),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(top: myHeight * 0.02, left: myWidth * 0.08),
                    child: Row(
                      children: [
                        Text(
                          'Historique des retraits',
                          style:
                          TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: myHeight * 0.02,
                  ),
                  Expanded(
                      child: ListView.builder(
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          return Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Padding(
                                  padding:
                                  EdgeInsets.symmetric(vertical: myHeight * 0.01),
                                  child: Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: myWidth * 0.06,
                                        vertical: myHeight * 0.02),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        color: Colors.white),
                                    child: Image.asset(
                                      'lib/images/2.png',
                                      height: myHeight * 0.05,
                                      // color: Color(0xff7847FC),
                                    ),
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Withdraw Money',
                                      style: TextStyle(
                                          fontSize: 18, fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: myHeight * 0.01),
                                    Text(
                                      'Yesterday',
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.normal,
                                          color: Colors.grey),
                                    ),
                                  ],
                                ),
                                Text(
                                  '\$ 100.000',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff7847FC),
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ))
                ],
              )),
        ));
  }

  Widget myText(String txt) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: myWidth * 0.03),
      child: Text(
        txt,
        style: TextStyle(
            fontSize: 27, color: Colors.white, fontWeight: FontWeight.normal),
      ),
    );
  }

  Widget myItemContiner(String title, String price) {
    double myHeight = MediaQuery.of(context).size.height;
    double myWidth = MediaQuery.of(context).size.width;
    return Container(
      padding: EdgeInsets.only(
          top: myHeight * 0.02,
          left: myWidth * 0.05,
          right: myWidth * 0.1,
          bottom: myHeight * 0.02),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
                color: Colors.grey, fontSize: 15, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: myHeight * 0.01,
          ),
          Row(
            children: [
              Text(
                '\F ',
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                price,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ],
          )
        ],
      ),
    );
  }

  Future<dynamic> confirmDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Apres validation si oui patienter 1h au max pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        openDialog('Montant', 'Retrait',()async {
                          if(soledPlus > int.parse(numeroController.text.trim()))
                          {
                            await getInserRetrait(int.parse(montantController.text.trim()),int.parse(numeroController.text.trim()) );
                          }
                          else
                          {
                            final result = await confirmDialog13(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }

                          }
                        }, montantController, numeroController, 'Numero'
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

  //champ

  Future<dynamic> validerRetrait(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Vous avez appuyer? si oui patienter 1h au max pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }
  //false

  Future<dynamic> confirmDialog1(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Atteindre 1000fr pour returer',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

  Future openDialog(hintText, titre, final Function()? onTap, TextEditingController MontantController, TextEditingController NumeroController ,hintText1 ) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(titre),
        content: Column(
          children: [
            TextField(
              controller:MontantController ,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade400),
                  ),
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  hintText: hintText,
                  hintStyle: TextStyle(color: Colors.grey[500])),
            ),
            TextField(
              controller: NumeroController,
              keyboardType: TextInputType.number,
              maxLength: 10,
              decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade400),
                  ),
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  hintText: hintText1,
                  hintStyle: TextStyle(color: Colors.grey[500])),
            ),
          ],
        ),
        actions: [
          GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(8),
              ),
              child: const Center(
                child: Text(
                  "Retirer",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          )
        ],
      ));


  Future<dynamic> confirmDialog13(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.red,
            ),
            title: const Text(
              'Montant superieur au solde',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }




  Future<dynamic> erreur(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.red,
            ),
            title: const Text(
              'Retrait en cours',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }


}

