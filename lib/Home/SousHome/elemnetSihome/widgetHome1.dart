import 'package:compte/model/pay.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../main.dart';
import '../../../model/like_profil.dart';
import '../../../repo/RepoUser.dart';

class ListHome1 extends StatefulWidget {
  ListHome1({super.key});
  late int Index; List
  @override
  State<ListHome1> createState() => _ListHome1State();
}

class _ListHome1State extends State<ListHome1> {
  late List<Like_profil> essai = [];
  var _userId = supabase.auth.currentSession?.user.id;
  final NoteRepository _noteRepository = NoteRepository();

  @override
  void initState() {
    // _getLikeAll();
    _getLikessai(_userId, widget.Index);
    //print(filteredNotes.length);
    // print(filteredNotes[0]);
    super.initState();
  }

  Future<void> _getLikessai(useru, lik) async {
    final videoUser = await _noteRepository.getVoirActive(useru, lik);
    print(videoUser);
    setState(() {
      essai = videoUser;
      print(essai);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () async  {
        await confirmActiver(context);
      },
      title: RichText(
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        text: TextSpan(
            text: '${essai[widget.Index].likes?.username} \n',
            style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 18,
                height: 1.5),
            children: [
              TextSpan(
                text: essai[widget.Index].likes?.content,
                style: const TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    height: 1.5),
              )
            ]),
      ),
      subtitle: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Text(
          'Edited: ${
          //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
              essai[widget.Index].likes?.created_at
          }',
          style: TextStyle(
              fontSize: 10,
              fontStyle: FontStyle.italic,
              color: Colors.grey.shade800),
        ),
      ),
      trailing: Column(
        children: [
          IconButton(
            onPressed: () async {
              final result = await confirmDialog(context);
              if (result != null && result) {
                // deleteNote(index);
              }
            },
            icon: const Icon(
              Icons.pending,
            ),
          ),
        ],
      ),
    );
  }

  Future<dynamic> confirmDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Vous avez appuyer? si oui patienter 24h pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

  Future<dynamic> confirmActiver(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'element en cour de verifucation si oui patienter 24h pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }
}
