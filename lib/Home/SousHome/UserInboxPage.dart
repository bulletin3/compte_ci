
import 'package:animate_do/animate_do.dart';
import 'package:compte/model/retrait.dart';
import 'package:compte/pagefictive.dart';
import 'package:compte/repo/RepoUser.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:iconsax/iconsax.dart';

import '../../main.dart';
import '../../pages/home_page.dart';
import '../../pages/login_page.dart';
import 'UserHomePage.dart';


class UserInboxPage extends StatefulWidget {
  const UserInboxPage({ Key? key }) : super(key: key);

  @override
  _UserInboxPageState createState() => _UserInboxPageState();
}

class _UserInboxPageState extends State<UserInboxPage> {
  late ScrollController _scrollController;
  var activerboutton = true;

  final TextEditingController numeroController = TextEditingController();
  final TextEditingController montantController = TextEditingController();
  RefreshController refreshController = RefreshController(initialRefresh: true);
  bool _isScrolled = false;
  late List<Retrait> retraitfield = [];
  late List<Retrait> Allretrait = [];
  final NoteRepository _noteRepository = NoteRepository();
  Future<void> _getUseSoldeProfil() async {
    final videoUser1 = await _noteRepository.getUserSolde();
    setState(() {
      soledPlus = (videoUser1[0].solde! )!;
      print('ange');
      print(soledPlus);
    });

  }

  Future<void>_refreshh()
  {
    return Future.delayed(Duration(seconds: 1));
  }


  Future<void> getInserRetrait(montant , numero, mode) async {
    print(montant );
    print(numero );
    print(mode );
    final videoUser = await _noteRepository.getRetrait(montant , numero, mode);
    //print(videoUser);
    await getrerefr();
    setState(() {
    //  retraitfield = videoUser;
      print(retraitfield);

    });
  }


  getrerefr() async {
    setState(() {
      Allretrait.clear();
    });
    await Future.delayed(const Duration(seconds: 2));

    setState(() {
      Allretrait.addAll(Allretrait);
    });
    refreshController.refreshCompleted();
  }

  var rav ;
  var rav1;
  Future<void> getAllRetrait() async {
    final videoUser1 = await _noteRepository.getRetraitAll();
    setState(() {
      Allretrait = videoUser1;
      rav = videoUser1.length;
    });
  }
  Future<void> getAllRetrait1() async {
    final videoUser1 = await _noteRepository.getRetraitAll1();
    setState(() {
      //Allretrait = videoUser1;
      rav1 = videoUser1.length;
    });
  }

  List<dynamic> _services = [
    ['Orange', Iconsax.export_1, Colors.blue, ],
    ['Mtn', Iconsax.import, Colors.pink],
    ['Moov', Iconsax.wallet_3, Colors.orange],
    ['Wave', Iconsax.more, Colors.green],
  ];

  List<dynamic> _transactions = [
    ['Amazon', 'https://img.icons8.com/color/2x/amazon.png', '6:25pm', '\$8.90'],
    ['Cash from ATM', 'https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/2x/external-atm-banking-and-finance-kiranshastry-lineal-color-kiranshastry.png', '5:50pm', '\$200.00'],
    ['Netflix', 'https://img.icons8.com/color-glass/2x/netflix.png', '2:22pm', '\$13.99'],
    ['Apple Store', 'https://img.icons8.com/color/2x/mac-os--v2.gif', '6:25pm', '\$4.99'],
    ['Cash from ATM', 'https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/2x/external-atm-banking-and-finance-kiranshastry-lineal-color-kiranshastry.png', '5:50pm', '\$200.00'],
    ['Netflix', 'https://img.icons8.com/color-glass/2x/netflix.png', '2:22pm', '\$13.99']
  ];

  var soledPlus = 0;

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_listenToScrollChange);
    _getUseSoldeProfil();
    getAllRetrait();
    getAllRetrait1();
    //print(getAllRetrait());
    super.initState();
  }

  void _listenToScrollChange() {
    if (_scrollController.offset >= 100.0) {
      setState(() {
        _isScrolled = true;
      });
    } else {
      setState(() {
        _isScrolled = false;
      });
    }
  }
  Future<void> _deconnexion() async
  {
    final userId = supabase.auth.currentUser!.id;
    final data = await supabase.auth.signOut();
    Get.off(LoginPage());

  }
  Future<void> onRefresh() async {
    await Future.delayed(
        Duration(seconds: 2)
    );
    if(!mounted) return;
    _scrollController = ScrollController();
    _scrollController.addListener(_listenToScrollChange);
    _getUseSoldeProfil();
    getAllRetrait();
  }


  final _advancedDrawerController = AdvancedDrawerController();

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          //backgroundColor: Colors.grey.shade100,
        backgroundColor: Theme.of(context).colorScheme.background,
          body: CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverAppBar(
                  expandedHeight: 250.0,
                  elevation: 0,
                  leading:  IconButton( onPressed: () async{
                    await _deconnexion();
                  }, icon: Icon(Icons.exit_to_app),
                  ),
                  pinned: true,
                  stretch: true,
                  toolbarHeight: 80,
                 // backgroundColor: Colors.white,
                 /* leading: IconButton(
                    color: Colors.black,
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: Icon(
                            value.visible ? Iconsax.close_square : Iconsax.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),*/
                 /* actions: [
                    IconButton(
                      icon: Icon(Iconsax.notification, color: Colors.grey.shade700),
                      onPressed: () {},
                    ),
                    IconButton(
                      icon: Icon(Iconsax.more, color: Colors.grey.shade700),
                      onPressed: () {},
                    ),
                  ],*/
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(40),
                      bottomRight: Radius.circular(40),
                    ),
                  ),
                  centerTitle: true,
                  title: AnimatedOpacity(
                    opacity: _isScrolled ? 1.0 : 0.0,
                    duration: const Duration(milliseconds: 500),
                    child: Column(
                      children: [
                        Text(
                          '\F ${soledPlus}',
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          width: 30,
                          height: 4,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      ],
                    ),
                  ),
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.pin,
                    titlePadding: const EdgeInsets.only(left: 20, right: 20),
                    title: AnimatedOpacity(
                      duration: const Duration(milliseconds: 500),
                      opacity: _isScrolled ? 0.0 : 1.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          FadeIn(
                            duration: const Duration(milliseconds: 500),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('\F', style: TextStyle( fontSize: 22),),
                                SizedBox(width: 3,),
                                Text('${soledPlus}',
                                  style: TextStyle(
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 50,),
                         /* FadeIn(
                            duration: const Duration(milliseconds: 500),
                            child: MaterialButton(
                              height: 30,
                              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                              onPressed: () {},
                              child: Text('Add Money', style: TextStyle(color: Colors.black, fontSize: 10),),
                              color: Colors.transparent,
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.grey.shade300, width: 1),
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                          ),*/
                          SizedBox(height: 10,),
                          Container(
                            width: 30,
                            height: 3,
                            decoration: BoxDecoration(
                              color: Colors.grey.shade800,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          SizedBox(height: 8,),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverList(
                    delegate: SliverChildListDelegate([
                      SizedBox(height: 10,),
                      Container(
                        padding: EdgeInsets.only(top: 20),
                        height: 115,
                        width: double.infinity,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _services.length,
                          itemBuilder: (context, index) {
                            return FadeInDown(
                              duration: Duration(milliseconds: (index + 1) * 100),
                              child: AspectRatio(
                                aspectRatio: 1,
                                child: GestureDetector(
                                  onTap: ()async {
                                    if(rav1 == 0)
                                      {
                                        if (_services[index][0] == 'Orange')  {
                                          if(soledPlus < 1000)
                                          {
                                            final result = await confirmDialog(context);
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                          else{

                                            final result = await confirmRetrait(context, 'Orange');
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }

                                        }
                                        if (_services[index][0] == 'Mtn')  {
                                          if(soledPlus < 1000)
                                          {
                                            final result = await confirmDialog(context);
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                          else{

                                            final result = await confirmRetrait(context, 'Mtn');
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                        }
                                        if (_services[index][0] == 'Moov')  {
                                          if(soledPlus < 1000)
                                          {
                                            final result = await confirmDialog(context);
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                          else{

                                            final result = await confirmRetrait(context, 'Moov');
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                        }
                                        if (_services[index][0] == 'Wave')  {
                                          if(soledPlus < 1000)
                                          {
                                            final result = await confirmDialog(context);
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                          else{

                                            final result = await confirmRetrait(context, 'Wave');
                                            if (result != null && result) {
                                              // deleteNote(index);
                                            }
                                          }
                                        }
                                      }
                                    else
                                      {
                                        if (_services[index][0] == 'Orange')  {
                                          final result = await erreur(context);
                                          if (result != null && result) {
                                            // deleteNote(index);
                                          }
                                        }
                                        if (_services[index][0] == 'Mtn')  {
                                          final result = await erreur(context);
                                          if (result != null && result) {
                                            // deleteNote(index);
                                          }
                                        }
                                        if (_services[index][0] == 'Moov')  {
                                          final result = await erreur(context);
                                          if (result != null && result) {
                                            // deleteNote(index);
                                          }
                                        }
                                        if (_services[index][0] == 'Wave')  {
                                          final result = await erreur(context);
                                          if (result != null && result) {
                                            // deleteNote(index);
                                          }
                                        }
                                      }
                                  },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: 60,
                                        height: 60,
                                        decoration: BoxDecoration(
                                          color: Colors.grey.shade900,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Center(
                                          child: Icon(_services[index][1], color: Colors.white, size: 25,),
                                        ),
                                      ),
                                      SizedBox(height: 10,),
                                      Text(_services[index][0], style: TextStyle( fontSize: 12),),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ])
                ),
                SliverFillRemaining(
                  child: Container(
                    padding: EdgeInsets.only(left: 20, right: 20, top: 30),
                    child: Column(
                      children: [
                        FadeInDown(
                          duration: Duration(milliseconds: 500),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Today', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600),),
                                SizedBox(width: 10,),
                                Text('\F ${soledPlus}', style: TextStyle( fontSize: 16, fontWeight: FontWeight.w700,)),
                              ]
                          ),
                        ),
                        Expanded(
                          child: ListView.builder(
                            padding: EdgeInsets.only(top: 20),
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: Allretrait.length,
                            itemBuilder: (context, index) {
                              return FadeInDown(
                                duration: Duration(milliseconds: 500),
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.shade200,
                                        blurRadius: 5,
                                        spreadRadius: 1,
                                        offset: Offset(0, 6),
                                      ),
                                    ],
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          Allretrait[index].mode== "Orange"?
                                          Image.asset('assets/orange.png', width: 50, height: 50,):
                                          Allretrait[index].mode == 'Mtn'?
                                          Image.asset('assets/mtn.png', width: 50, height: 50,):
                                          Allretrait[index].mode == 'Wave'?
                                          Image.asset('assets/wave.png', width: 50, height: 50,):
                                          Image.asset('assets/moov.png', width: 50, height: 50,),

                                          SizedBox(width: 15,),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text('${Allretrait[index].numero}' , style: TextStyle(color: Colors.grey.shade900, fontWeight: FontWeight.w500, fontSize: 14),),
                                              SizedBox(height: 5,),
                                              Text('${Allretrait[index].montant}' , style: TextStyle(color: Colors.grey.shade500, fontSize: 12),),
                                            ],
                                          ),
                                        ],
                                      ),
                                Allretrait[index].payer == false?
                                Text('non payer'
                                        , style: TextStyle(color: Colors.red, fontSize: 16, fontWeight: FontWeight.w700),):
                                Text('payer'
                                  , style: TextStyle(color: Colors.green, fontSize: 16, fontWeight: FontWeight.w700),)
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ]
          )
      );
  }

  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
  Future<dynamic> confirmDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'atteindre 1000fr pour pourvoir retirer',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

  //retrait
  Future openDialog(hintText, titre, final Function()? onTap, TextEditingController MontantController, TextEditingController NumeroController ,hintText1 ) => showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(titre),
        content: Column(
          children: [
            TextField(
              controller:MontantController ,
              keyboardType: TextInputType.number,
              style: TextStyle(color: ThemeData().colorScheme.onBackground),
              decoration: InputDecoration(
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade400),
                  ),
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  hintText: hintText,
                  hintStyle: TextStyle(color: Colors.grey[500])),
            ),
            TextField(
             // cursorColor: Colors.black,
              controller: NumeroController,
              keyboardType: TextInputType.number,
              maxLength: 10,
              style: TextStyle(color: ThemeData().colorScheme.onBackground),
              decoration: InputDecoration(
                  enabledBorder:  OutlineInputBorder(
                    borderSide: BorderSide(),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey.shade400),
                  ),
                  fillColor: Colors.grey.shade200,
                  filled: true,
                  hintText: hintText1,
                  hintStyle: TextStyle(color: Colors.grey[500])),
            ),
          ],
        ),
        actions: [
          activerboutton == true?
          GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(8),
              ),
              child: const Center(
                child: Text(
                  "Retirer",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ):
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(8),
              ),
              child: const Center(
                child: Text(
                  "Retirer",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 50,),
          GestureDetector(
            onTap: () {
              Get.offAll( HomePage1());
            },
            child: Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(8),
              ),
              child: const Center(
                child: Text(
                  "Valider",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          )
        ],
      ));
  // si sommes est inferrieur aux montants retirer
  Future<dynamic> confirmRetrait(BuildContext context, String mode) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Apres validation si oui patienter 1h au max pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () async {
                        openDialog('Montant', 'Retrait',()async {
                          if(soledPlus > int.parse(montantController.text.trim()))
                          {

                            await getInserRetrait(int.parse(montantController.text.trim()),int.parse(numeroController.text.trim()), '${mode}' );
                            activerboutton == false;

                          }
                          else
                          {
                            final result = await confirmDialog(context);
                            if (result != null && result) {
                              // deleteNote(index);
                            }

                          }
                        }, montantController, numeroController, 'Numero',
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }
  //retrait deja en cour


  Future<dynamic> erreur(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.red,
            ),
            title: const Text(
              'Retrait en cours',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

}