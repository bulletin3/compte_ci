import 'dart:math';

import 'package:compte/model/like_profil.dart';
import 'package:compte/model/pay.dart';
import 'package:compte/model/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import '../../main.dart';
import '../../repo/RepoUser.dart';
import '../../utils/dart.dart';
import 'lieedit.dart';
import 'likeedit/edit.dart';

class UserPlusPage extends StatefulWidget {
  const UserPlusPage({super.key});

  @override
  State<UserPlusPage> createState() => _UserPlusPageState();
}

class _UserPlusPageState extends State<UserPlusPage> {
  late List<Likes> filteredNotes = [];
  late List<Like_profil> essai = [];
  late List<Like_profil> verifiAffiche = [];
  var _userId = supabase.auth.currentSession?.user.id;
  final NoteRepository _noteRepository = NoteRepository();
  bool sorted = false;

  @override
  void initState() {
    _getLikeAll();
    _getLikeAllAldulte();
    _getLikeAll1();
    //_getLikessai(_userId, 1);
    print(filteredNotes.length);
    // print(filteredNotes[0]);
    super.initState();
  }

  late List<Likes> filteredNotes1 = [];
  late List<Profiles> UserSection = [];

  late int ageT = 0;
  var likeT;
  Future<void> _getLikeAll1() async {
    final videoUser = await _noteRepository.getUserSolde();
    print('object');
    print(videoUser[0].age);
    setState(() {
      UserSection = videoUser;
      ageT = UserSection[0].age!;
      // print(UserSection[0]);
    });
  }

  Future<void> _getLikeAllAldulte() async {
    final videoUser = await _noteRepository.getLikeAldulte(3);
    //print(videoUser);
    setState(() {
      filteredNotes1 = videoUser;
      //print(filteredNotes);
    });
  }


  Future<void> _getLikeAll() async {
    final videoUser = await _noteRepository.getLike(3);
    print(videoUser);
    setState(() {
      filteredNotes = videoUser;
      //print(filteredNotes);
    });
  }
  Future<void> onRefresh() async {
    await Future.delayed(
        Duration(seconds: 2)
    );
    if(!mounted) return;
    this._getLikeAll();
    this._getLikeAllAldulte();
    this._getLikeAll1();
  }



  List<Likes> sortNotesByModifiedTime(List<Likes> notes) {
    if (sorted) {
      notes.sort((a, b) => a.created_at!.compareTo(b.created_at!));
    } else {
      notes.sort((a, b) => a.created_at!.compareTo(b.created_at!));
    }

    sorted = !sorted;

    return notes;
  }


  getRandomColor() {
    Random random = Random();
    return backgroundColors[random.nextInt(backgroundColors.length)];
  }

  void onSearchTextChanged(String searchText) {
    setState(() {
      filteredNotes = filteredNotes
          .where((note) =>
      note.content!.toLowerCase().contains(searchText.toLowerCase()) ||
          note.username!.toLowerCase().contains(searchText.toLowerCase()))
          .toList();
    });
  }

  void deleteNote(int index) {
    setState(() {
      Likes note = filteredNotes[index];
      filteredNotes.remove(note);
      filteredNotes.removeAt(index);
    });
  }

  _getLikessai(useru, lik) async {
    final videoUser1 = await _noteRepository.getVoirActive(useru, lik);
    return videoUser1;
    print(videoUser1.length);
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ageT >17?
      Container(
        child: Scaffold(
          //backgroundColor: Colors.grey.shade900,
          // backgroundColor: Colors.grey.shade900,
          //backgroundColor: Colors.white,
          backgroundColor: Theme.of(context).colorScheme.background,
          body: Padding(
            padding: const EdgeInsets.fromLTRB(16, 40, 16, 0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      'Facebook',
                      style: TextStyle(fontSize: 30),
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            filteredNotes = sortNotesByModifiedTime(filteredNotes);
                          });
                        },
                        padding: const EdgeInsets.all(0),
                        icon: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.grey.shade800.withOpacity(.8),
                              borderRadius: BorderRadius.circular(10)),
                          child: const Icon(
                            Icons.sort,
                            color: Colors.white,
                          ),
                        ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  onChanged: onSearchTextChanged,
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(vertical: 12),
                    hintText: "Recherche Post",
                    hintStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.search,
                      color: Colors.grey,
                    ),
                    fillColor: Colors.grey.shade800,
                    filled: true,
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(color: Colors.transparent),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(color: Colors.transparent),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                    child: ListView.builder(
                      padding: const EdgeInsets.only(top: 30),
                      itemCount: filteredNotes.length,
                      itemBuilder: (context, index) {
                        final _Like = filteredNotes![index];
                        return Card(
                          margin: const EdgeInsets.only(bottom: 20),
                          color: getRandomColor(),
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child:ListTile(
                              onTap: () async {
                                final result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          EditScreen1(note: _Like,)),
                                );
                                if (result != null) {
                                  setState(() {
                                    int originalIndex = filteredNotes.indexOf(_Like);

                                    filteredNotes[originalIndex] = Likes(
                                      id: filteredNotes[originalIndex].id,
                                      username: result[0],
                                      content: result[1],
                                      created_at: DateTime.now() as String,
                                    );

                                    filteredNotes[index] = Likes(
                                      id: filteredNotes[originalIndex].id,
                                      username: result[0],
                                      content: result[1],
                                      created_at: DateTime.now() as String,
                                    );
                                  });
                                }
                              },
                              title: RichText(
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                    text: '${_Like.username} \n',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        height: 1.5),
                                    children: [
                                      TextSpan(
                                        text: _Like.content,
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 14,
                                            height: 1.5),
                                      )
                                    ]),
                              ),
                              subtitle: Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  'Edited: ${
                                  //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
                                      _Like.type?.nom}',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.grey.shade800),
                                ),
                              ),
                              trailing: Column(
                                children: [
                                  IconButton(
                                    onPressed: () async {
                                      final result = await confirmDialog(context);
                                      if (result != null && result) {
                                        // deleteNote(index);
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.pending,
                                    ),
                                  ),
                                ],
                              ),
                            ),


                            /*ListTile(
                          onTap: () async {
                            final result = await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      EditScreen1(note: _Like)),
                            );
                            if (result != null) {
                              setState(() {
                                int originalIndex = filteredNotes.indexOf(_Like);

                                filteredNotes[originalIndex] = Likes(
                                  id: filteredNotes[originalIndex].id,
                                  username: result[0],
                                  content: result[1],
                                  created_at: DateTime.now() as String,
                                );

                                filteredNotes[index] = Likes(
                                  id: filteredNotes[originalIndex].id,
                                  username: result[0],
                                  content: result[1],
                                  created_at: DateTime.now() as String,
                                );
                              });
                            }
                          },
                          title: RichText(
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            text: TextSpan(
                                text: '${_Like.username} \n',
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    height: 1.5),
                                children: [
                                  TextSpan(
                                    text: _Like.content,
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14,
                                        height: 1.5),
                                  )
                                ]),
                          ),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text(
                              'Edited: ${
                              //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
                              _Like.type?.nom}',
                              style: TextStyle(
                                  fontSize: 10,
                                  fontStyle: FontStyle.italic,
                                  color: Colors.grey.shade800),
                            ),
                          ),
                          trailing: Column(
                            children: [
                              IconButton(
                                onPressed: () async {
                                  final result = await confirmDialog(context);
                                  if (result != null && result) {
                                    // deleteNote(index);
                                  }
                                },
                                icon: const Icon(
                                  Icons.pending,
                                ),
                              ),
                            ],
                          ),
                        ),*/
                            /* ListTile(
                              onTap: () async  {
                               await confirmActiver(context);
                              },
                              title: RichText(
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                text: TextSpan(
                                    text: '${_Like.username} \n',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        height: 1.5),
                                    children: [
                                      TextSpan(
                                        text: _Like.content,
                                        style: const TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.normal,
                                            fontSize: 14,
                                            height: 1.5),
                                      )
                                    ]),
                              ),
                              subtitle: Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Text(
                                  'Edited: ${
                                  //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
                                      _Like.type?.nom
                                  }',
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontStyle: FontStyle.italic,
                                      color: Colors.grey.shade800),
                                ),
                              ),
                              trailing: Column(
                                children: [
                                  IconButton(
                                    onPressed: () async {
                                      final result = await confirmDialog(context);
                                      if (result != null && result) {
                                        // deleteNote(index);
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.pending,
                                    ),
                                  ),
                                ],
                              ),
                            ),*/
                          ),
                        );
                      },
                    ))
              ],
            ),
          ),
          /*floatingActionButton: FloatingActionButton(
            onPressed: () async {
             /* final result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => const EditScreen(),
                ),
              );

              if (result != null) {
                setState(() {
                  sampleNotes.add(Note(
                      id: sampleNotes.length,
                      title: result[0],
                      content: result[1],
                      modifiedTime: DateTime.now()));
                  filteredNotes = sampleNotes;
                });
              }*/
            },
            elevation: 10,
            backgroundColor: Colors.grey.shade800,
            child: const Icon(
              Icons.add,
              size: 38,
            ),
          ),*/
        ),
      )
      :  Container(
        width: double.infinity,
        child: Scaffold(
          // backgroundColor: Colors.grey.shade900,
          backgroundColor: Theme.of(context).colorScheme.background,
          body: Padding(
            padding: const EdgeInsets.fromLTRB(16, 40, 16, 0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Facebook',
                      style: TextStyle(fontSize: 30, ),
                    ),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            filteredNotes1 = sortNotesByModifiedTime(filteredNotes1);
                          });
                        },
                        padding: const EdgeInsets.all(0),
                        icon: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.grey.shade800.withOpacity(.8),
                              borderRadius: BorderRadius.circular(10)),
                          child: const Icon(
                            Icons.sort,
                            color: Colors.white,
                          ),
                        ))
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                TextField(
                  onChanged: onSearchTextChanged,
                  style: const TextStyle(fontSize: 16, color: Colors.white),
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(vertical: 12),
                    hintText: "Recherche Post",
                    hintStyle: const TextStyle(color: Colors.grey),
                    prefixIcon: const Icon(
                      Icons.search,
                      color: Colors.grey,
                    ),
                    fillColor: Colors.grey.shade800,
                    filled: true,
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(color: Colors.transparent),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: const BorderSide(color: Colors.transparent),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                    child: ListView.builder(
                      padding: const EdgeInsets.only(top: 30),
                      itemCount: filteredNotes1.length,
                      itemBuilder: (context, index) {
                        final _Like = filteredNotes1![index];
                        return Card(
                          margin: const EdgeInsets.only(bottom: 20),
                          color: getRandomColor(),
                          elevation: 3,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child:ListTile(
                                onTap: () async {
                                  final result = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            EditScreen1(note: _Like,)),
                                  );
                                  if (result != null) {
                                    setState(() {
                                      int originalIndex = filteredNotes1.indexOf(_Like);

                                      filteredNotes[originalIndex] = Likes(
                                        id: filteredNotes1[originalIndex].id,
                                        username: result[0],
                                        content: result[1],
                                        created_at: DateTime.now() as String,
                                      );

                                      filteredNotes1[index] = Likes(
                                        id: filteredNotes1[originalIndex].id,
                                        username: result[0],
                                        content: result[1],
                                        created_at: DateTime.now() as String,
                                      );
                                    });
                                  }
                                },
                                title: RichText(
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  text: TextSpan(
                                      text: '${_Like.username} \n',
                                      style: const TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18,
                                          height: 1.5),
                                      children: [
                                        TextSpan(
                                          text: _Like.content,
                                          style: const TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 14,
                                              height: 1.5),
                                        )
                                      ]),
                                ),
                                subtitle: Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(
                                    'Edited: ${
                                    //DateFormat('EEE MMM d, yyyy h:mm a').format(filteredNotes[index].type.nom)
                                        _Like.type?.nom}',
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontStyle: FontStyle.italic,
                                        color: Colors.grey.shade800),
                                  ),
                                ),

                                trailing:   Column(
                                  children: [
                                    IconButton(
                                      onPressed: () async {
                                        final result = await confirmDialog(context);
                                        if (result != null && result) {
                                          // deleteNote(index);
                                        }
                                      },
                                      icon: const Icon(
                                        Icons.pending,
                                      ),
                                    )
                                  ],
                                )
                            ),
                          ),
                        );
                      },
                    )
                )
              ],
            ),
          ),
          /*floatingActionButton: FloatingActionButton(
            onPressed: () async {
             /* final result = await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => const EditScreen(),
                ),
              );

              if (result != null) {
                setState(() {
                  sampleNotes.add(Note(
                      id: sampleNotes.length,
                      title: result[0],
                      content: result[1],
                      modifiedTime: DateTime.now()));
                  filteredNotes = sampleNotes;
                });
              }*/
            },
            elevation: 10,
            backgroundColor: Colors.grey.shade800,
            child: const Icon(
              Icons.add,
              size: 38,
            ),
          ),*/
        ),
      ),
    );
  }

  Future<dynamic> confirmDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'Vous avez appuyer? si oui patienter 24h pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, true);
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'Yes',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'No',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }

  Future<dynamic> confirmActiver(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            backgroundColor: Colors.grey.shade900,
            icon: const Icon(
              Icons.info,
              color: Colors.grey,
            ),
            title: const Text(
              'element en cour de verifucation si oui patienter 24h pour etre credité',
              style: TextStyle(color: Colors.white),
            ),
            content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        Navigator.pop(context, false);
                      },
                      style:
                      ElevatedButton.styleFrom(backgroundColor: Colors.red),
                      child: const SizedBox(
                        width: 60,
                        child: Text(
                          'ok',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                      )),
                ]),
          );
        });
  }
}
