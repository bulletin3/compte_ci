import 'package:compte/Home/SousHome/UserProfilePage.dart';
import 'package:compte/components/my_button.dart';
import 'package:compte/components/my_textfield.dart';
import 'package:compte/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:scroll_date_picker/scroll_date_picker.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


class Profiladd extends StatefulWidget {
  const Profiladd({super.key});

  @override
  State<Profiladd> createState() => _ProfiladdState();
}

class _ProfiladdState extends State<Profiladd> {
  DateTime _dateTime = DateTime.now();
  DateTime _selectedDate = DateTime.now();



  final TextEditingController CentreController = TextEditingController();

  var age ;

  Future<void> addNomCompte() async
  {

    showDialog(
      context: context,
      builder: (context) {
        return const Center(
          child: CircularProgressIndicator(),
        );
      },
    );
   try{
     final username = supabase.auth.currentUser!.userMetadata!['username'];
     final userId = supabase.auth.currentUser!.id;
     final numero = supabase.auth.currentUser!.userMetadata!['numero'];
     final parrain = supabase.auth.currentUser!.userMetadata!['parrain'];
     final email = supabase.auth.currentUser?.email;
     //final username = supabase.auth.currentUser.userMetadata
     // print(sessions);
     await supabase.from('profiles').update({
       'age': age,
       'dateAni': _selectedDate.toString(),
       'centre': CentreController.text.trim()
     }).eq('id', userId);
     if(!mounted) return;

     Navigator.push(
       context,
       MaterialPageRoute(builder: (context) => UserProfilePage()),);
   }on AuthException catch (e)
   {
     print('connexion');
     ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Incorecte')));
     Get.offAll(UserProfilePage());
     print(e);
   }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [

                SizedBox(height: 10,),
                Column(
                  children: [
                    Text("Date de naissance "),
                    SizedBox(
                      height: 250,
                      child: ScrollDatePicker(
                        selectedDate: _selectedDate,
                        locale: Locale('fr'),
                        onDateTimeChanged: (DateTime value) {
                          setState(() {
                            _selectedDate = value;
                            print(_selectedDate);
                            print(_dateTime);
                            age = _dateTime.year - _selectedDate.year;
                            print(age);
                          });
                        },
                      ),
                    ),
                    /// Showcase second image source
                    // SizedBox(
                    //   height: 250,
                    //   child: ScrollDatePicker(
                    //     selectedDate: _selectedDate,
                    //     locale: Locale('ko'),
                    //     scrollViewOptions: DatePickerScrollViewOptions(
                    //       year: ScrollViewDetailOptions(
                    //         label: '년',
                    //         margin: const EdgeInsets.only(right: 8),
                    //       ),
                    //       month: ScrollViewDetailOptions(
                    //         label: '월',
                    //         margin: const EdgeInsets.only(right: 8),
                    //       ),
                    //       day: ScrollViewDetailOptions(
                    //         label: '일',
                    //       )
                    //     ),
                    //     onDateTimeChanged: (DateTime value) {
                    //       setState(() {
                    //         _selectedDate = value;
                    //       });
                    //     },
                    //   ),
                    // ),
                  ],
                ),

                SizedBox(height: 10,),
                MyTextField(
                  controller: CentreController,
                  hintText: "centre d'interret",
                  obscureText: false,
                ),
                SizedBox(height: 20,),
                MyButton2(
                  onTap: () async {
                    await addNomCompte();
                    //print(_dateTime);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
