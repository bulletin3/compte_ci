import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../main.dart';
import '../pages/home_page.dart';
import '../pages/login_page.dart';
import '../utils/GlobalColor.dart';

class Index extends StatefulWidget {
  const Index({super.key});

  @override
  State<Index> createState() => _IndexState();
}

class _IndexState extends State<Index> {

    @override
    void initState() {
      _redirect();
      super.initState();
    }

    Future<void> _redirect() async
    {
      await Future.delayed(Duration.zero);
      final session = supabase.auth.currentSession;
      print(session);
      print('vir vour');
      if(session != null){
        Timer(Duration(seconds: 2), () {
          Get.to(HomePage());
        } );
      }
      else
      {
        Timer(Duration(seconds: 2), () {
          Get.off(LoginPage());
        } );
      }
    }

    @override
    Widget build(BuildContext context) {

      return Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/logo.png')
            ],
          ),
        ),
      );
    }
  }
